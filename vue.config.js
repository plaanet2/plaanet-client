module.exports = {
  lintOnSave: false,
  "transpileDependencies": [
    "vuetify"
  ],

  devServer: {     
      https: false,
  },
}