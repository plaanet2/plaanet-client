export const getReferralLink = (nodeName, referrerUid) => {
  return `${window.location.origin}/?ref=${encodeURIComponent(nodeName)}%3A${encodeURIComponent(referrerUid)}`;
}
