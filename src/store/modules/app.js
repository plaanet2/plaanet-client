import axios from "axios";

const state = () => ({
  isLoading: true,
  drawerOpen: true,
  popupEmailOpen: false,
  popupPushNotifOpen: false,
  popupScopeOpen: false,

  
  activity: {
    pages: [],
    assemblies: [],
    events: [],
  },
  activityLoading: {
    pages: false,
    assemblies: false,
    events: false,
  },
  
});

const actions = {
  setDrawerOpen({ commit }, open) {
    commit("setDrawerState", open);
  },
  setAppLoading({ commit }, isLoading) {
    commit('setLoadingState', isLoading);
  },
  setPopupEmailOpen({ commit }, isOpen) {
    commit('setPopupEmailOpen', isOpen);
  },
  setPopupPushNotifOpen({ commit }, isOpen) {
    commit('setPopupPushNotifOpen', isOpen);
  },
  setPopupScopeOpen({ commit }, isOpen) {
    commit('setPopupScopeOpen', isOpen);
  },
  updateActivity({ commit }, activity) {
    commit('updateActivity', activity);
  },
  setActivityLoading({ commit }, isLoading) {
    commit('setActivityLoading', isLoading);
  },
  async fetchActivity({ commit }, params) {
    let key = params.key
    commit('setActivityLoading', { key: key, loading: true })

    await commit('setActivity', { key: key, pages: [] });
    params.origin = "activity"
    axios.post('/page/get-map-pages/desync', params)
          .then((res)=>{
            commit('updateActivity', { key: key, pages: res.data.mapPages });
            commit('setActivityLoading', { key: key, loading: false })
          })

  }
};

const mutations = {
  setDrawerState(state, open) {
    state.drawerOpen = open;
  },
  setLoadingState(state, isLoading) {
    state.isLoading = isLoading;
  },
  setPopupEmailOpen(state, isOpen) {
    state.popupEmailOpen = isOpen;
  },
  setPopupPushNotifOpen(state, isOpen) {
    state.popupPushNotifOpen = isOpen;
  },
  setPopupScopeOpen(state, isOpen) {
    state.popupScopeOpen = isOpen;
  },
  setActivity(state, data) {
    state.activity[data.key] = data.pages
  },
  updateActivity(state, data) {
    //console.log("updateActivity", data)
    let arr = state.activity[data.key].concat(data.pages)
    state.activity[data.key] = orderMapPage(arr, "dateConnected").reverse()
  },
  setActivityLoading(state, data) {
    state.activityLoading[data.key] = data.loading
  }
};

const getters = {};

export default {
  namespaced: true,
  getters,
  state,
  actions,
  mutations
};


function orderMapPage(array, key){
  //console.log("orderMapPage", array.length, key)
  let fRes = []
  array.forEach((obj1) => {
      if(fRes.length > 0){
          let spliced = false
          fRes.map((obj2, i) => {
              let d1 = new Date(obj1[key])
              let d2 = new Date(obj2[key])
              if(d1 < d2 && !spliced){
                  fRes.splice(i, 0, obj1)
                  spliced = true
              }
          })
          if(!spliced) fRes.push(obj1)
      }else{ 
          fRes.push(obj1)
      }
  })
  return fRes
}
