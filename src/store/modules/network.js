import axios from "axios";

const config = require('../../config/' + process.env.NODE_ENV)

// initial state
const state = () => ({
  localName: null,
  localBaseUrl: null,
  localUrl: null,
  localPort: null,
  localSocketPort: null,
  nodes: [],
})

// getters
const getters = {
  websocketUrl: (state) => {
    const socketUrl = new URL(
      [
        state.localUrl
      ]
    );
    const wsProtocol = socketUrl.protocol === "https:" ? "wss" : "ws";
    return `${wsProtocol}://${socketUrl.host}:${state.localSocketPort}`;
  },
  nodesByName: (state) => {
    return state.nodes.reduce((acc, node) => {
      acc[node.name] = node;
      return acc;
    }, {});
  },
  activeNodes: (state) => {
    let nb = 0
    state.nodes.forEach((node)=>{
      if(!node.lockOut 
      && !node.unreachable 
      && node.isActive) nb++
    })
    return nb
  }
};

// actions
const actions = {
  async fetchNodes({ dispatch, commit, state }, opts = { refresh: false }) {
    try {
      if (state.nodes.length !== 0 && !opts.refresh) {
        return;
      }

      const {data} = await axios.get('/instance/get-network');
      if (data.error) {
        throw new Error(data.errorMsg);
      }
      if (window.isDev) {
        console.log('Fetched local node & network data:', data);
      }

      commit('update', {
        localName: data.localNode.name,
        localLocation: data.localNode.city,
        localBaseUrl: data.localNode.url + ":" + data.localNode.port,
        localUrl: data.localNode.url,
        localPort: data.localNode.port,
        localSocketPort: data.localNode.portSocket,
        nodes: data.network
      });

      // Update user context.
      await dispatch('auth/fetchUserContext', null, { root: true });
    } catch (err) {
      if (window.isDev) {
        console.log('Cannot load nodes. Err:', err.message);
      }
    }
  },

  async init(context){
    if (window.isDev) {
      console.log("config.root_node", config.root_node);
    }

    let baseUrl = localStorage.getItem('baseUrl');
    console.log("NETWORK INIT baseUrl 1", baseUrl);
    // for backwards compatibility
    // replace synja node url
    if(baseUrl != null && baseUrl.indexOf("synja") !== -1){
      baseUrl = "https://plaanet.synja.org:444"
      localStorage.setItem("baseUrl", baseUrl)
    }

    //if no baseUrl found : load config root node
    if(baseUrl == null || baseUrl == '')
      baseUrl = `${config.root_node.url}:${config.root_node.port}`;
    
    console.log("NETWORK INIT baseUrl 2", baseUrl);

    //save it in LS
    localStorage.setItem("baseUrl", baseUrl)
    //init axios baseURL
    axios.defaults.baseURL = baseUrl;

    context.commit('init', {
      localName: '',
      localLocation: '',
      localBaseUrl: baseUrl,
      localUrl: config.root_node.url,
      localPort: config.root_node.port,
      localSocketPort: config.root_node.socketPort,
      nodes: [],
    })
  },
  update(context, data){
    context.commit('update', {
      localName: data.localNode.name,
      localLocation: data.localNode.city,
      localBaseUrl: data.localNode.url + ":" + data.localNode.port,
      localUrl: data.localNode.url,
      localPort: data.localNode.port,
      localSocketPort: data.localNode.socketPort,
      nodes: data.network
    })
  },
  selectNode(context, data){
    context.commit('selectNode', {
      localName: data.name,
      localLocation: data.city,
      localBaseUrl: data.url + ":" + data.port,
      localUrl: data.url,
      localPort: data.port,
      localSocketPort: data.portSocket
    })
    localStorage.setItem("baseUrl", data.url+":"+data.port)
    axios.defaults.baseURL = data.url+":"+data.port
    localStorage.setItem("nodeSelected", true)
  },
}

// mutations
const mutations = {
  init(state, network) {
    state.localName = network.localName
    state.localLocation = network.localLocation
    state.localBaseUrl = network.localBaseUrl
    state.localUrl = network.localUrl
    state.localPort = network.localPort
    state.localSocketPort = network.localSocketPort
    state.nodes = network.nodes
  },
  update(state, network) {
    state.localName = network.localName
    state.localLocation = network.localLocation
    state.localBaseUrl = network.localBaseUrl
    state.localUrl = network.localUrl
    state.localPort = network.localPort
    state.localSocketPort = network.localSocketPort
    state.nodes = network.nodes
  },
  selectNode(state, node) {
    state.localName = node.localName
    state.localLocation = node.localLocation
    state.localBaseUrl = node.localBaseUrl
    state.localUrl = node.localUrl
    state.localPort = node.localPort
    state.localSocketPort = node.localSocketPort
  },
  setNodes(state, nodes) {
    state.nodes = nodes;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}