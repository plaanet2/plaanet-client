import axios from "axios";
import { v4 as uuid } from 'uuid';
//import core from "../../plugins/core"

import featureDetectNotification from "../../core/utils/featureDetectNotification";

const state = () => ({
  hasPermission: false,
  shouldRequestPermission: false,
  permissionRequestLoading: false,
  drawerOpen: false,
  nbNotReadNotif: 0,
  nbNotReadMessage: 0,
  notifications: [],
  flashNotifications: [],
  invitesRoles: [],
  pendingsRoles: [],
});

const actions = {
  async init({ commit }) {
    if (featureDetectNotification() === false) {
      console.log('WARN: No window.Notification support in this browser.');
      return;
    }

    const hasPermission = window.Notification.permission === 'granted';
    console.log("hasPermission for flash notif ?", hasPermission)
    commit('setShouldRequestPermission', !hasPermission);
    commit('setHasPermission', hasPermission);

    if (window.isDev) {
      console.log("Notifications permission request status:", hasPermission);
    }
  },
  async requestPermission({ dispatch, commit }) {
    commit('setPermissionRequestLoading', true)

    // WARNING: This doesn't resolve until user accepted/declined the permission.
    const permissionStatus = await window.Notification.requestPermission();
    const hasPermission = permissionStatus === 'granted';

    if (window.isDev) {
      console.log("Notifications permission request status:", hasPermission);
    }

    // Whatever response we get, make sure Chrom* browsers stores the status.
    if ('permission' in window.Notification === false) {
      window.Notification = permissionStatus;
    }

    commit('setShouldRequestPermission', !hasPermission);
    commit('setHasPermission', hasPermission);

    if (hasPermission) {
      dispatch('notifications/sendNotification', {
        title: this.$vuetify.lang.t('$vuetify.stream.ItIsWorking'),
        body: this.$vuetify.lang.t('$vuetify.stream.notificationsActivated') + ' !',
        image: 'https://plaanet.io/img/nexxo-logo.956e38e4.png',
        durationMs: 3 * 1000,
      }, {root:true});
    }

    commit('setPermissionRequestLoading', false);
  },
  dismissPermissionRequest({ commit }) {
    commit('setShouldRequestPermission', false);
  },
  sendNotification() {
    return;
    /*
    if (!notif.body || !notif.title) {
      (window.isDev) && console.error('Sending a Notification requires a title and a body.');
      return;
    }

    if (window.isDev) {
      console.log('Sending a notification. Notif:', notif);
    }

    if (!notif._id) {
      notif._id = uuid();
    }

    commit('addFlashNotification', notif);

    //remove lock to end this feature
    let lock = true
    if (!lock && state.hasPermission && !notif.appOnly) {
      const n = new window.Notification(
        notif.title,
        {
          tag: notif.tag || 'plaanet',
          body: notif.body,
          data: {
            url: notif.url,
          },
          icon: notif.image || "https://plaanet.io/logo-earth.png",
          vibrate: [200, 100, 200],
          timestamp: notif.timestamp || Math.floor(Date.now()),
          requireInteraction: false
        }
      );

      n.addEventListener('click', (ev) => {
        if (!ev || !ev.target || !ev.target.data || !ev.target.data.url) {
          return;
        }

        console.log('notification routing to:', notif.url)
        window.location.pathname = ev.target.data.url;
      });
    }*/
  },
  clearFlashNotification({ commit }, id) {
    commit('clearFlashNotification', id);
  },
  async setDrawerOpen({ dispatch, rootState, commit }, open) {
    commit("setDrawerState", open);
    const { data } = await axios.post('/page/update-date-read-notif')
    if(data.error == false){
      await dispatch('auth/update_date_read_notif', data.date, { root: true })
      commit("countNotRead", { 
        dateReadNotif: rootState.auth.profile.dateReadNotif, 
        dateReadPrivate: rootState.auth.profile.dateReadPrivate
      });
    }
  },
  async updateDatePrivate({ dispatch, rootState, commit }){
    const { data } = await axios.post('/page/update-date-read-private')
    if(data.error == false){
      await dispatch('auth/update_date_read_private', data.date, { root: true })
      commit("countNotRead", { 
        dateReadNotif: rootState.auth.profile.dateReadNotif, 
        dateReadPrivate: rootState.auth.profile.dateReadPrivate
      });
    }
  },
  async fetchNotifications({ commit, rootState }) {
    const { data } = await axios.get("/user/notifications");
    if (!data.success) {
      // TODO: Store error in store.
      return;
    }

    commit("setNotificationsState", data.data.notifications);
    commit("countNotRead", { 
      dateReadNotif: rootState.auth.profile.dateReadNotif, 
      dateReadPrivate: rootState.auth.profile.dateReadPrivate
    });
  },
  async fetchInviteRoles({ commit, rootState }) {
    const { data } = await axios.get("/page/get-invite-roles");
    //console.log("fetchInviteRoles", data)
    if (data.error == false) {
      commit("setInvitesRoles", data.invites)
      commit("countNotRead", { 
        dateReadNotif: rootState.auth.profile.dateReadNotif, 
        dateReadPrivate: rootState.auth.profile.dateReadPrivate
      });
    }
  },
  async fetchPendingRoles({ commit, rootState }) {
    const { data } = await axios.get("/page/get-pending-roles");
    console.log("fetchPendingRoles", data)
    if (data.error == false) {
      commit("setPendingsRoles", data.pendings)
      commit("countNotRead", { 
        dateReadNotif: rootState.auth.profile.dateReadNotif, 
        dateReadPrivate: rootState.auth.profile.dateReadPrivate
      });
    }
  },
  async addPendingRoles({ commit }, role) {
    console.log("addPendingRoles", role)
    commit("addPendingRoles", role)
  },
  async newNotification({ commit, rootState }, notif) {
    commit("newNotification", notif);
    commit("countNotRead", { 
      dateReadNotif: rootState.auth.profile.dateReadNotif, 
      dateReadPrivate: rootState.auth.profile.dateReadPrivate
    });

    
  },
  clearNotifications({ commit, rootState }) {
    commit("clearNotifications");
    commit("countNotRead", { 
      dateReadNotif: rootState.auth.profile.dateReadNotif, 
      dateReadPrivate: rootState.auth.profile.dateReadPrivate
    });
  },
  markNotificationRead({ commit, rootState }, id) {
    commit("setNotificationRead", id);
    commit("countNotRead", { 
      dateReadNotif: rootState.auth.profile.dateReadNotif, 
      dateReadPrivate: rootState.auth.profile.dateReadPrivate
    });
  }
};

const mutations = {
  setPermissionRequestLoading(state, isLoading) {
    state.permissionRequestLoading = isLoading;
  },
  setHasPermission(state, hasPermission) {
    state.hasPermission = hasPermission;
  },
  setShouldRequestPermission(state, shouldRequestPermission) {
    state.shouldRequestPermission = shouldRequestPermission;
  },
  setDrawerState(state, open) {
    state.drawerOpen = open;
  },
  setNotificationsState(state, notifications) {
    state.notifications = notifications.reverse();
  },
  addFlashNotification(state, notif) {
    notif._id = uuid();
    
    console.log("#NOTIF DEBUG", state.flashNotifications, state.flashNotifications.length)
    if(state.flashNotifications.length == 0) 
      state.flashNotifications = [notif]
    else{
      state.flashNotifications = [
        ...state.flashNotifications,
        notif,
      ]; 
    }
  },
  removeFlashNotification(state, id) {
    state.flashNotifications = [
      ...state.flashNotifications.filter(notification => notification._id !== id),
    ];
  },
  clearFlashNotifications(state) {
    state.flashNotifications = [];
  },
  newNotification(state, notif) {

    let found = -1
    state.notifications.forEach((noti, i) => {
    // console.log("check exists", notif.verb, notif.verb
    //         , notif.whatObj.uid, notif.whatObj.uid, notif.aboutObj.uid, notif.aboutObj.uid)
        if(noti.aboutObj != null)
        if(noti.verb == notif.verb && 
          (noti.aboutObj.uid == notif.aboutObj.uid)
          ) found = i
        
        if(noti.aboutObj == null)
        if(noti.verb == notif.verb && 
          (noti.whatObj.uid == notif.whatObj.uid)
          ) found = i
    })

    // if (window.isDev) {
    //   console.log("check found notif to splice", found)
    // }

    if(found > -1) state.notifications.splice(found, 1)
    
    state.notifications.unshift(notif)

    //core.showPushNotif(notif)
  },
  clearNotifications(state) {
    state.notifications = [];
  },
  setNotificationRead(state, id) {
    state.notifications = [
      ...state.notifications.filter(notif => {
        return notif._id !== id;
      })
    ];
  },
  countNotRead(state, data) {
    let nb = 0

    state.notifications.forEach((notif)=>{
      if(notif.updated > data.dateReadPrivate 
        && notif.verb == 'NEW_PRIVATE_MSG') nb++
    })

    state.nbNotReadMessage = nb;    
    
    nb = 0
    state.notifications.forEach((notif)=>{
    if(notif.updated > data.dateReadNotif
      && notif.verb != 'NEW_PRIVATE_MSG') nb++
    })

    nb += state.invitesRoles.length
    nb += state.pendingsRoles.length

    state.nbNotReadNotif = nb
  },

  setInvitesRoles(state, invites){
    state.invitesRoles = invites
  },
  setPendingsRoles(state, pendings){
    state.pendingsRoles = pendings
  },
  addPendingRoles(state, role){
    state.pendingsRoles.push(role)
  },
  
};

const getters = {
  hasNotifications(state) {
    return state.notifications.length > 0;
  },
  unreadNotificationsCount(state) {
    let nb = 0
    if (window.isDev) {
      console.log("nbPrivateMsg context");
    }

    state.notifications.forEach((notif)=>{
    if (window.isDev) {
      console.log(
        "nbPrivateMsg context",
        notif.updated,
        state.auth.profile.dateReadPrivate,
        notif.updated > this.$store.state.auth.profile.dateReadPrivate
      );
    }

    if(notif.updated > state.auth.profile.dateReadPrivate 
    && notif.verb == 'NEW_PRIVATE_MSG') nb++
    //if(window.isDev) { console.log("context.dateReadPrivate", notif, this.context.userPage.dateReadPrivate); }

    })

    if (window.isDev) {
      console.log("nbPrivateMsg final", nb);
    }

    return nb;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}