import axios from "axios";

const state = () => ({
  isLoading: false,
  posts: [],
  pages: [],
  countPages: 0,
  mode : 'posts', //posts, pages, send
  origin: 'local',//local, follows, favorites
  pageType: '',   //user, group, assembly, event
  searchStr: '',
  lastStreamLength: 0, //size of the last stream received (to know if its the end)
  lastNbPerPage: 0,
  radiusMax: 0,
  rootMarker: {
    lat: 47.16685851396646,
    lng: -2.1697435379028323,
  },
  rootMarkerInited: false,
  map: {
    userMainPosition: [0, 0],
    show: false
  },
  streamHistory: [],

  pagesHistory: [],
  lastPagesLength: 0,
  lastPagesNbPerPage: 0,

  currentPageNumber: 1
});

const actions = {
  async init({ state, commit }) {
    //if(window.isDev) { console.log('#STREAM init', state.radiusMax) }
    //commit('init', {  userMainPosition: rootState.auth.profile.coordinates })
    
    if(state.radiusMax == 0){
      let radius = 600
      try{
        const res = await axios.get('/instance/get-radius-max')
        if(res.status == 404){
          //console.log("Error : /instance/get-radius-max return null (404). Using default radiusMax value", radius)
        }else{
          radius = res.data.radiusMax
          //console.log("Success : /instance/get-radius-max exist. Using api radiusMax value", radius)
        }
        await commit('setRadiusMax', radius)
      }catch(e){
        await commit('setRadiusMax', radius)
        //console.log("Catch Error : /instance/get-radius-max not exist. Using default radiusMax value", radius)
      }

      //if(window.isDev) { console.log('#STREAM init radius', radius) }
    }

  },
  receivedStreamPost({ commit, rootState }, data){
    let blacklistedPages = rootState.auth.profile.blacklist
    commit('addPosts', { stream : data.stream, blacklist: blacklistedPages })
    commit('setLastStreamLength', data.stream.length)
    commit('setLastNbPerPage', data.nbPerPage)
  },
  receivedMapPages({ commit }, data){
    commit('addPages', data.entities)
    commit('setLastStreamLength', data.entities.length)

    commit('updateCountPages', data.totalRes)
    //commit('setLastNbPerPage', data.nbPerPage)
  },
  async fetchPost({ state, commit, rootState }, params) {
    try {
      commit('setIsLoading', true)

      params.accessToken == rootState.auth.user.access_token

      if(params.pageNumber == 1){ await commit('updatePosts', [])}
      
      
      if(window.isDev) { console.log('#STREAM fetchPost params', params) }
      //const {data} = await 
      axios.post('/publication/get-stream', params)
           .then(async (res)=>{
              if (res.data.error) { throw new Error(res.data.errorMsg) }
              
              let blacklistedPages = rootState.auth.profile.blacklist
              await commit('addPosts', { stream : res.data.stream, blacklist: blacklistedPages })
              await commit('setLastStreamLength', res.data.stream.length)
              await commit('setLastNbPerPage', res.data.nbPerPage)

              setTimeout(()=>{ commit("setIsLoading", false) }, 1000)
              if(window.isDev) { console.log('#STREAM fetchPost new state', state.posts.length, state.posts) }
            })

    } catch (err) {
      if (window.isDev) {
        console.log('Cannot load stream. Err:', err.message);
      }
    }
  },
  async fetchPages({ state, commit }, params) {
    try {
      if(params.pageNumber == 1) {
        await commit('updatePages', [])
        await commit('initCountPages')
      }
      
      commit('setCurrentPageNumber', params.pageNumber)
      commit("setIsLoading", true);

      params.origin = "stream"
      if(window.isDev) { console.log('#STREAM fetchPages params', params) }
      
      axios.post('/page/get-map-pages/desync', params)
           .then(async (res)=>{
              if (res.data.error) { throw new Error(res.data.errorMsg) }
              if(window.isDev) { console.log('#STREAM fetchPages params', res.data) }

              await commit('addPages', res.data.mapPages)
              await commit('updateCountPages', res.data.totalRes)
              await commit('setLastStreamLength', res.data.mapPages.length)
              await commit('setLastNbPerPage', res.data.nbPerPage)

              setTimeout(()=>{ commit("setIsLoading", false) }, 1000)

              if(window.isDev) { console.log('#STREAM fetchPages new state', state.pages.length, state.posts) }
            })
      
    } catch (err) {
      if (window.isDev) { console.log('Cannot load stream. Err:', err.message) }
    }
  },
  async fetchCountPages({ state, commit }, params) {
    try {
      commit('setIsLoading', true);
      //if (state.pages.length !== 0 && !opts.refresh) { return }

      if(window.isDev) { console.log('#STREAM fetchCountPages params', params) }
      const {data} = await axios.post('/page/get-count-pages', params)
      if (data.error) { throw new Error(data.errorMsg) }

      await commit('updateCountPages', data.count)
      if(window.isDev) { console.log('#STREAM fetchCountPages new state', state.pages.length, state.posts) }
    } catch (err) {
      if (window.isDev) {
        console.log('Cannot load stream. Err:', err.message);
      }
    } finally {
      commit('setIsLoading', false);
    }
  },
  async initPosts({ commit }) {
    if(window.isDev) { console.log('#STREAM initPosts') }
    commit('initPosts')
  },
  async initPages({ commit }) {
    if(window.isDev) { console.log('#STREAM initPages') }
    commit('initPages')
  },
  async setMode({ commit }, mode) {
    if(window.isDev) { console.log('#STREAM setMode', mode) }
    commit('setMode', mode)
  },
  async setOrigin({ commit }, origin) {
    if(window.isDev) { console.log('#STREAM setOrigin', origin) }
    commit('setOrigin', origin)
  },
  async setPageType({ commit }, pageType) {
    if(window.isDev) { console.log('#STREAM setPageType', pageType) }
    commit('setPageType', pageType)
  },
  async setSearchStr({ commit }, searchStr) {
    if(window.isDev) { console.log('#STREAM setSearchStr', searchStr) }
    commit('setSearchStr', searchStr)
  },
  async showMap({ commit }, show) {
    if(window.isDev) { console.log('#STREAM showMap', show) }
    commit('showMap', show)
  },
  async addNewPost({ commit }, post) {
    if(window.isDev) { console.log('#STREAM addNewPost', post) }
    commit('addNewPost', post)
  },
  async setRootMarker({ commit }, marker) {
    if(window.isDev) { console.log('#STREAM setRootMarker', marker) }
    commit('setRootMarker', marker)
  },
  
};

const mutations = {
  setIsLoading: (state, isLoading) => state.isLoading = isLoading,
  init(state, params) {
    state.map.userMainPosition = params.userMainPosition
  },
  initPosts(state) {
    state.posts = []
  },
  initPages(state) {
    state.pages = []
    state.countPages = 0
  },
  updatePosts(state, stream) {
    state.posts = stream
    if(stream.length == 0) state.streamHistory = []
  },
  updatePages(state, stream) {
    state.pages = stream
    if(stream.length == 0) state.pagesHistory = []
  },
  addPosts(state, data) {
    let arr = state.posts.concat(data.stream)
    arr = clearBlacklist(arr, data.blacklist)
    state.posts = orderMapPage(arr, "created").reverse()
  },
  addPages(state, stream) {
    let arr = state.pages.concat(stream)
    state.pages = orderMapPage(arr, "dateConnected").reverse()
  },
  initCountPages(state) {
    state.countPages = 0
  },
  updateCountPages(state, count) {
    //console.log("updateCountPages", state.currentPageNumber, state.countPages, count)
    if(state.currentPageNumber == 1)
      state.countPages += count
  },
  setMode(state, mode) {
    state.mode = mode
    if(mode == 'send'){
      state.map.show = true
      state.pageType = 'user'
    }
  },
  setOrigin(state, origin) {
    state.origin = origin
  },
  setPageType(state, pageType) {
    state.mode = 'pages'
    state.pageType = pageType
  },
  setSearchStr(state, searchStr) {
    state.searchStr = searchStr
    if(searchStr == null) state.searchStr = ""
  },
  showMap(state, show) {
    state.map.show = show
  },
  setLastStreamLength(state, len){
    state.lastStreamLength = len
    state.streamHistory.push(len)
  },
  setLastNbPerPage(state, nbPerPage){
    state.lastNbPerPage = nbPerPage
  },
  addNewPost(state, post){
    state.posts.unshift(post)
  },
  setRootMarker(state, marker){
    state.rootMarker = marker
    state.rootMarkerInited = true
  },
  setRadiusMax(state, radius){
    state.radiusMax = radius
  },


  setLastPagesLength(state, len){
    state.lastPagesLength = len
    state.pagesHistory.push(len)
  },
  setLastPagesNbPerPage(state, nbPerPage){
    state.lastPagesNbPerPage = nbPerPage
  },

  setCurrentPageNumber(state, pageNumber){
    state.currentPageNumber = pageNumber
  }

}


// getters
const getters = {
  showStream: (state) => {
   return state.mode != "map"
  },
  showFormEditPost: (state) => {
   return state.mode == "send"
  },
  draggable: (state) => {
   return state.mode != "posts"
  },
  endOfStream: (state) => {
    console.log("endOfStream", state.lastStreamLength, state.lastNbPerPage, state.lastStreamLength < state.lastNbPerPage)
    
   },
}

function orderMapPage(array, key){
  console.log("orderMapPage", array.length, key)
  let fRes = []
  array.forEach((obj1) => {
      if(fRes.length > 0){
          let spliced = false
          fRes.map((obj2, i) => {
              let d1 = new Date(obj1[key])
              let d2 = new Date(obj2[key])
              if(d1 < d2 && !spliced){
                  fRes.splice(i, 0, obj1)
                  spliced = true
              }
          })
          if(!spliced) fRes.push(obj1)
      }else{ 
          fRes.push(obj1)
      }
  })
  return fRes
}

function clearBlacklist(arr, blacklist){
  let res = []
  let blacklisted = false
  arr.forEach((post)=>{
    blacklisted = false
    blacklist.forEach((page)=>{
      if(post.signed.uid == page.uid)
        blacklisted = true
    })
    if(!blacklisted) res.push(post)
    //else console.log("Hidden post from blacklisted page", post.signed.name, post.signed.uid)
  })
  return res
}


export default {
  namespaced: true,
  getters,
  state,
  actions,
  mutations
}
