import Vue from "vue";
import Vuex from "vuex";

import app from "./modules/app";
import auth from "./modules/auth";
import network from "./modules/network";
import notifications from "./modules/notifications";
import postComposer from "./modules/postComposer";
import stream from "./modules/stream";
import room from "./modules/room";
import live from "./modules/live";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    app,
    auth,
    network,
    notifications,
    postComposer,
    stream,
    room,
    live,
  },
});
