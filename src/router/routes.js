const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("../views/home.vue"),
    meta: { guestOnly: true }
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/login.vue"),
    meta: { guestOnly: true }
  },
  {
    path: "/register",
    name: "register",
    component: () => import("../views/register.vue"),
    meta: { guestOnly: true }
  },
  {
    path: "/manifest",
    name: "manifest",
    component: () => import("../views/manifest.vue")
  },
  {
    path: "/manifest-en",
    name: "manifest",
    component: () => import("../views/manifest-en.vue")
  },
  {
    path: "/donate",
    name: "donate",
    component: () => import("../views/donate.vue")
  },
  {
    path: "/plan",
    name: "plan",
    component: () => import("../views/plan.vue")
  },
  {
    path: "/info-nodes",
    name: "info-nodes",
    component: () => import("../views/info-nodes.vue")
  },
  {
    path: "/info-system",
    name: "info-system",
    component: () => import("../views/info-system.vue")
  },
  {
    path: "/select-node",
    name: "select-node",
    component: () => import("../views/select-node.vue"),
    meta: { guestOnly: true }
  },
  {
    path: "/clear-local-storage",
    name: "clear-local-storage",
    component: () => import("../views/clear-local-storage.vue")
  },
  {
    path: "/first-step",
    name: "first-step",
    component: () => import("../views/first-step.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/map",
    name: "map",
    component: () => import("../views/main.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/stream-explore",
    name: "stream-explore",
    component: () => import("../views/main.vue"),
    meta: { requiresAuth: true },
    props: { explore: true }
  },
  {
    path: "/stream",
    name: "stream",
    component: () => import("../views/main.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/private/sendto",
    name: "private-sendto",
    component: () => import("../views/formPrivate.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/private/sendto/:uid/:name",
    name: "private-sendto-uid",
    component: () => import("../views/formPrivate.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/private",
    name: "private",
    component: () => import("../views/private.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/blacklist",
    name: "blacklist",
    component: () => import("../views/blacklist.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/follows",
    name: "follows",
    component: () => import("../views/follows.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/contacts",
    name: "contacts",
    component: () => import("../views/contacts.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/list/:type",
    name: "list-by-type",
    component: () => import("../views/pageList.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/post/:postUid",
    name: "single-post",
    component: () => import("../views/SinglePost.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/page/:pageUid/:mode",
    name: "single-page-mode",
    component: () => import("../views/page.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/page/:pageUid/",
    name: "single-page",
    component: () => import("../views/page.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/page-roles/:pageUid/",
    name: "page-roles",
    component: () => import("../views/pageRoles.vue"),
    meta: { requiresAuth: true }
  },  
  {
    path: "/live/:pageUid?/:categoryUid?/:channelUid?/",
    name: "live",
    component: () => import("../views/live.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/settings/:pageUid/",
    name: "page-settings",
    component: () => import("../views/pageSettings.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/room-all",
    name: "room-all",
    component: () => import("../views/room.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/room/:roomUid/",
    name: "room",
    component: () => import("../views/room.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/room/publish/:roomUid/",
    name: "room-publish",
    component: () => import("../views/createSurvey.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/room/survey/:surveyUid/",
    name: "survey",
    component: () => import("../views/survey.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/room/duplicate-survey/:surveyUid/:roomUid/",
    name: "survey",
    component: () => import("../views/createSurvey.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/room/edit-survey/:surveyUid/",
    name: "survey",
    component: () => import("../views/createSurvey.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/find-assembly",
    name: "find-assembly",
    component: () => import("../views/find-assembly.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/find-assembly/:createAssembly",
    name: "find-assembly-create",
    component: () => import("../views/find-assembly.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/create-page/",
    name: "createPage",
    component: () => import("../views/create-page.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/create/:pageType",
    name: "createPageType",
    component: () => import("../views/create-page.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/myPages",
    name: "myPages",
    component: () => import("../views/myPages.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: '/me/invite-friends',
    name: 'invite-friends',
    component: () => import('../views/InviteFriends.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: "/confidentiality",
    name: "confidentiality",
    component: () => import("../views/informations/confidentiality.vue")
  },
  {
    path: "/legals",
    name: "legals",
    component: () => import("../views/informations/legals.vue")
  },
  {
    path: "/cgu",
    name: "cgu",
    component: () => import("../views/informations/cgu.vue")
  },
  {
    path: "/visit",
    name: "visit",
    component: () => import("../views/visit.vue")
  },
  {
    path: "/faq",
    name: "faq",
    component: () => import("../views/faq.vue")
  },
  {
    path: "/admin",
    name: "admin",
    component: () => import("../views/admin/dashboard.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/admin/visitors",
    name: "admin-visitors",
    component: () => import("../views/admin/dashVisitors.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/admin/page-modo",
    name: "admin-page-modo",
    component: () => import("../views/admin/page-modo.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/status",
    name: "status",
    component: () => import("../views/admin/dashboard.vue")
  },
  {
    path: "/admin/network",
    name: "admin-network",
    component: () => import("../views/admin/node-network.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/admin/socket",
    name: "admin-socket",
    component: () => import("../views/admin/socket.vue"),
    meta: { requiresAuth: true }
  },
  {
    path: "/admin/version-mobile",
    name: "admin-version-mobile",
    component: () => import("../views/admin/version-mobile.vue"),
    meta: { requiresAuth: true } 
  },
  {
    path: "/admin/emails",
    name: "admin-emails",
    component: () => import("../views/admin/emails.vue"),
    meta: { requiresAuth: true } 
  },
  {
    path: "/last-version-mobile-app",
    name: "last-version-mobile-app",
    component: () => import("../views/last-version-mobile-app.vue"),
  },
  {
    path: "/download-app",
    name: "download-app",
    component: () => import("../views/download-app.vue"),
  },
  {
    path: "/boot-node",
    name: "boot-node",
    component: () => import("../views/boot-node.vue")
  },
  {
    path: "/reload-view",
    name: "boot-node",
    component: () => import("../views/reload-view.vue")
  },
  {
    path: "/404",
    name: "not-found",
    alias: "*",
    component: () => import("../views/not-found.vue")
  },
  {
    path: "*",
    redirect: "/404"
  },
];

export default routes;