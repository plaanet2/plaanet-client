const config = require('../config/' + process.env.NODE_ENV)

import axios from "axios";
import store from '../store'
//import Vue from "vue";

let core = {
    rand: 0,
    initRand: function(){
        core.rand = Math.random(1000)
    },
    iName: function(uid) {
        if(uid==null) return ''
        let i = uid.indexOf(':')
        return uid.substr(0, i)
    },
    iKey: function(uid){
        if(uid == null) return uid
        let i = uid.indexOf(':')
        return uid.substr(i+1, uid.length)
    },
    extractTags : function(text){
        let tags = text.match(/(^|\s)(#[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/ig);
        if(tags != null)
        tags.forEach((tag, key) => {
            tags[key] = tag.replace(" ", "");
        })

        return tags
    },
    pathToAPK(){ return config.root_node.url + ":" + config.root_node.port + '/apk/android-plaanet-app.apk' },
    avatarUrl : function(pageUid, refresh=true){
        if(pageUid == null) return ''
        if(store.state.network == null) return
        if (core.rand == 0) core.initRand()
        
        let iName = core.iName(pageUid)
        let node = store.getters['network/nodesByName'][iName]

        if(node == null) return ""

        let forceRefresh = ""
        let randate = new Date()
        randate = randate.toISOString().substr(0,13)
        if(refresh) forceRefresh = "?" + randate // Math.random()
        let url = node.url + ":" + node.port + "/uploads/avatar/" + pageUid + ".png" + forceRefresh

        return url
    },
    avatarUrlDefault : function(pageUid){
        if(pageUid == null) return ''
        if(core.rand==0) core.initRand()

        let iName = core.iName(pageUid)
        let node = store.getters['network/nodesByName'][iName]
        let url = node.url + ":" + node.port + "/default-avatar.png"

        return url // + "?" + core.rand
    },
    postImageUrl : function(postUid){
        if(postUid == null) return ''
        if(core.rand==0) core.initRand()

        let iName = core.iName(postUid)
        let node = store.getters['network/nodesByName'][iName]
        let url = node.url + ":" + node.port + "/uploads/post/"

        return url
    },
    postPdfUrl : function(pageUid, pdfName){
        if(pageUid == null) return ''
        if(core.rand==0) core.initRand()

        let node = store.getters['network/nodesByName'][core.iName(pageUid)]
        let url = node.url + ":" + node.port + "/uploads/pdf/" + pageUid + "/" + pdfName
        return url
    },
    iconPageType: function(type){
        if(type == 'user') return 'account'
        if(type == 'group') return 'account-group-outline'
        if(type == 'assembly') return 'hexagon-multiple-outline'
        if(type == 'event') return 'calendar-clock'
    },
    colorPageType: function(type){
        if(type == 'user') return 'yellow'
        if(type == 'group') return 'red'
        if(type == 'assembly') return 'purple'
        if(type == 'event') return 'orange'
    },

    isFollowing: function(state, pageUid){
        let found = false
        if(state.profile.follows==null) return false
            state.profile.follows.forEach((page) => {
            if(page != null && page.uid == pageUid) found = true
        })
        return found
    },
    isMyContact: function(state, pageUid){
        if(state.user.pages.length < 1) return false
        let found = false
        state.profile.contacts.forEach((page) => {
            if(page.uid == pageUid) found = true
        }); return found
    },
    isBlacklisted: function(state, pageUid){
        let found = false
        if(state.profile.blacklist==null) return false
        state.profile.blacklist.forEach((page) => {
            if(page.uid == pageUid) found = true
        })
        return found
    },
    completeText: function(txt, hashtags, nl2brX=true){
        if(txt == null) return ""

        txt = txt.replace(/<[^>]*>/g, '');

        let allLinks = txt.match(/\bhttps?:\/\/\S+/gi)

        if(allLinks != null)
        allLinks.forEach((link) => {
           let l = link
           if(link.indexOf('fbclid=') > -1) link = link.slice(0, link.indexOf('fbclid=')-1)
           txt = txt.replace(l, link) 
           txt = txt.replace(link, '<a href="'+link+'" target="_blank" rel="noopener">'+link+'</a>')
        })
        
        // if(hashtags != null && hashtags.length > 0)
        //     hashtags.forEach((tag)=>{
        //         txt = txt.replace(tag, '') //' <span data-tag="'+tag+'" class="span-hashtag">'+tag+'</span>')
        //     })
        let regex = /(^|\s)(@[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ_\d-]+)/ig
        let mentions = txt.match(regex);
        //console.log("ccompleteText", mentions)
        if(mentions != null && mentions.length > 0)
        txt = txt.replaceAll(regex, ' <span class="span-hashtag">$&</span>')
        
        // mentions.forEach((mention)=>{
        //     txt = txt.replace(mention, ' <span data-tag="'+mention+'" class="span-hashtag">'+mention+'</span>')
        // })

        if(nl2brX) txt = core.nl2br(txt)
        return txt
    },
    nl2br: function(str, is_xhtml){
      var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>';
      return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    },
    addEmojiToPost: function(notif, originalPost){
        if(originalPost != null && originalPost.uid == notif.aboutObj.uid){
            notif.whatObj.author = notif.authors[0]
            if(notif.whatObj.parentCommentId == null){
              originalPost.comments.forEach((com) => { 
                if(com._id == notif.whatObj.uid){
                  let found = false
                  com.emojis.forEach((emo) => { 
                    if(emo.name == notif.whatObj.emoji){
                      found = true
                      if(notif.whatObj.posAdded > -1)
                        emo.users.splice(notif.whatObj.posAdded, notif.whatObj.posAdded+1)
                      else
                        emo.users.push(notif.whatObj.author.uid)
                    }
                  })
                  if(found == false){
                    com.emojis.push({ name: notif.whatObj.emoji, users: [notif.whatObj.author.uid]})
                  }
                }
              })
            }else{
              originalPost.comments.forEach((com1) => { 
                com1.answers.forEach((com) => { 
                  if(com._id == notif.whatObj.uid){
                    let found = false
                    com.emojis.forEach((emo) => { 
                      if(emo.name == notif.whatObj.emoji){
                        found = true
                        if(notif.whatObj.posAdded > -1)
                          emo.users.splice(notif.whatObj.posAdded, notif.whatObj.posAdded+1)
                        else
                          emo.users.push(notif.whatObj.author.uid)
                      }
                    })
                    if(found == false){
                      com.emojis.push({ name: notif.whatObj.emoji, users: [notif.whatObj.author.uid]})
                    }
                  }
                })
              })
            }
        }
        return originalPost
    },
    addCommentToPost: function(notif, originalPost){
        if(originalPost != null && originalPost.uid == notif.aboutObj.uid){
            //if(window.isDev) { console.log("#WS info : a user is watching the parent post", data.aboutObj.uid); }
            notif.whatObj.author = notif.authors[0]
            notif.whatObj.answers = []
            if(notif.whatObj.parentCommentId != null){
              originalPost.comments.forEach((com, x) => {
                if(com._id == notif.whatObj.parentCommentId){
                  originalPost.comments[x].answers.push(notif.whatObj)
                }
              })
            } else {
              originalPost.comments.push(notif.whatObj)
            }
        }
        console.log("originalPost.comments", originalPost.comments)
        return originalPost
    },
    isRole: (userPageUid, page, role) => { if(page.roles == null) return false
      let found = false
      page.roles[role].forEach((p)=>{  if(p.uid == userPageUid) found = true })
      return found
    },
    incStat: function(url){
      axios.post('/instance/inc-stat-url', { domaineName: config.domaineName, url: url })
    },
    countMention: function(channel, myUid){
      if(channel.mentions == null) return 0

      let count = 0
      channel.dateLastOpen.forEach((dataOpen) => {
        if(dataOpen.uid == myUid)
        channel.mentions.forEach((mention) => {
          //console.log("countMention", count, mention.date, dataOpen.date, mention.date > dataOpen.date, mention.uid == dataOpen.uid, mention.uid == myUid)
          if(mention.uid == dataOpen.uid){
            //console.log("countMention", channel.name, count, new Date(mention.date), new Date(dataOpen.date), mention.date > dataOpen.date)
            if(new Date(mention.date) > new Date(dataOpen.date)) count++
          }
        })
      })
      //console.log("-------- countMention", channel.name, count)
      return count
    },
    liveUserUid: function(uid1, uid2){
      let uid = ""
      if(uid1 < uid2)  uid = uid1 + uid2
      else             uid = uid2 + uid1
      return uid
    },
    getPageUidRef(pageUid, userPageUid, pageType){
      let pageUidRef = pageUid
      if(pageType == "user"){
        pageUidRef = core.liveUserUid(pageUid, userPageUid)
      }
      return pageUidRef
    },
    upAlertLiveOnMember: function(state, currentPageUid, parentPageUid, active){
      console.log("#LIVE store.state.auth.profile.memberPages", store.state.auth.profile.memberPages.length)
      store.state.auth.profile.memberPages.forEach((friend, f)=>{
        console.log("#LIVE found? upAlertLiveOnMember", 
        core.liveUserUid(friend.uid, currentPageUid) == parentPageUid,
        core.liveUserUid(friend.uid, currentPageUid), parentPageUid,
        store.state.auth.profile.memberPages[f].name)
        if(core.liveUserUid(friend.uid, currentPageUid) == parentPageUid){
          store.state.auth.profile.memberPages[f].alert = active
          //console.log("#LIVE ALERT : FOUND USER", core.liveUserUid(friend.uid, state.currentPage.uid), state.currentPage.roles.editor[f].uid)
        }
      })
    },
    sortMember: function(members){
      return members.sort((x, y) => { return (x.name.toUpperCase() < y.name.toUpperCase()) ? -1 : 1; })
                    .sort((x, y) => { return (x.type < y.type)? 0 : x.type ? -1 : 1; })
                    //.sort((x, y) => { return (x.alert === y.alert)? 0 : x.alert? -1 : 1; })
    },
    getLastOpenChannelUid: function(myUid, categories){
      let catUid = ""
      let chanUid = ""
      let res = []
      categories.forEach((cat)=>{
        cat.channels.forEach((chan)=>{
          chan.dateLastOpen.forEach((lastO)=>{
            //console.log("------chan.dateLastOpen", lastO.uid, myUid, lastO.uid == myUid)
            if(lastO.uid == myUid)
              res.push({ chanUid: chan.uid, catUid: cat.uid, date: lastO.date })
          })
        })
      })
      let resSorted = res.sort((x, y) => { return (x.date > y.date) ? -1 : 1; })

      if(res.length == 0) 
        resSorted = [{  chanUid: categories[0].channels[0].uid, 
                        catUid: categories[0].uid }]

      console.log("getLastOpenChannelUid", res, resSorted, catUid, chanUid)
      return { lastOpenCategoryUid: resSorted[0].catUid, lastOpenChannelUid: resSorted[0].chanUid }
    },

    /*showPushNotif: function (notif){

      //ne pas signaler quand on enlève un like, dislike, etc
      if((notif.verb == "LIKES"   || notif.verb == "DISLIKES" 
        ||  notif.verb == "VIEWERS" || notif.verb == "FAVORITES") 
            && notif.whatObj.posAdded > -1) return false

      // const notificationImage = "https://plaanet.io/img/nexxo-logo.png";
      const notificationText = this.getPushNotifText(notif);

      Vue.notification.show('Plaanet - Notification', {
        body: notificationText
      }, {})

      // Notification.requestPermission( function() {
      //   new window.Notification(
      //     `Plaanet - Notification`,
      //     {
      //       body: notificationText,
      //       icon: notificationImage,
      //       vibrate: [200, 100, 200],
      //       timestamp: Math.floor(new Date(notif.created)),
      //       requireInteraction: true,
      //     }
      //   )
      // });

    },*/

    getPushNotifText: function(notif){
      let texts = {
        "COMMENT" : this.$vuetify.lang.t('$vuetify.notif.NewCommentFrom') + ' ' + notif.authors[0].name,
        "LIKES"    : notif.authors[0].name + ' ' + this.$vuetify.lang.t('$vuetify.notif.likedPost'),
        "DISLIKES"    : notif.authors[0].name + ' ' + this.$vuetify.lang.t('$vuetify.notif.dislikedPost'),
        "EMOJI" : notif.authors[0].name + ' ' + this.$vuetify.lang.t('$vuetify.notif.reactedMessage'),
        "FAVORITES" : notif.authors[0].name + ' ' + this.$vuetify.lang.t('$vuetify.notif.addedFavoritesPost'),
        "VIEWERS" : notif.authors[0].name + ' ' + this.$vuetify.lang.t('$vuetify.notif.isreadingPost'),
        "NEW_ROLE_ACCEPTED" : notif.authors[0].name + ' ' + this.$vuetify.lang.t('$vuetify.notif.acceptedYourDemand'),
        "NEW_PRIVATE_MSG" : notif.authors[0].name + ' ' + this.$vuetify.lang.t('$vuetify.notif.sentMessage'),
      }
      return texts[notif.verb] != null ? texts[notif.verb] : this.$vuetify.lang.t('$vuetify.notif.WatchPlaanetNotifs')
    }

}

export default core