Constatant :

    l'ingérance de nos gouvernements à assurer les besoins fondamentaux et inaliénables du peuple,
    que les mesures mises en place engendrent des conséquences désastreuses pour l'immense majorité d'entre nous, mais également pour l'ensemble du Vivant,
    que les seules véritables réponses apportées lorsque le peuple conteste sont la censure, la violence et les sanctions disproportionnées.

Nous considérons qu’il n’est plus possible de continuer à leur accorder notre confiance pour décider de notre avenir et celui de nos enfants.

Nous proclamons :

    la mise en place d’une nouvelle structure démocratique, pour et par le peuple,
    qu’il est de notre devoir de participer à ce qu'il convient maintenant d'appeler la Résilience, chacun à notre échelle, dans tous nos villages et toutes nos villes,
    oeuvrer pour la construction, ensemble et par nos propres moyens, des conditions d'un monde viable pour les générations à venir en favorisant le Vivant sous toutes ses formes,
    souhaiter la convergence et synergie de ceux qui se reconnaitront dans ce manifeste,
    pouvoir jouir de nos droits naturels et inaliénables d'Êtres Vivants Libres.

Nous nous engageons à :

    utiliser Plaanet pour la mise en contact et en lien afin de partager des informations, chercher des solutions communes, vers une convergence et synergie, dans la plus grande légitimité des Êtres Vivants Libres jouissant de leurs droits naturels et inaliénables,
    insuffler une dynamique nouvelle pour favoriser le Vivant et inciter les Êtres à oeuvrer en faveur de notre émancipation collective,

    utiliser Plaanet pour nous retrouver et nous organiser localement, mettre en place nos projets résilients, proposer nos propres sondages et référendums pour ainsi décider nous-mêmes des mesures que nous souhaitons voir mises en œuvre,

    nous respecter les uns les autres, et à user de notre intelligence individuelle, mais aussi collective, pour faire avancer nos projets de la meilleure des façons, dans le respect et la bienveillance, chacun engageant sa responsabilité personnelle.

    écarter la peur de nos esprits, et recourir à la désobéissance joyeuse et pacifique, lorsque les droits fondamentaux du Vivant sont en danger.pecter les uns les autres, et à user de notre intelligence pour faire avancer les discussions de la meilleure des façons.
